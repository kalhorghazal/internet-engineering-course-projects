# Internet Engineering Course Projects

1. CA1: Maven, JSON Domain, Unit Test(JUnit), Git
2. CA2: Simple Web Server, Javalin
3. CA3: Tomcat, MVC, Java Servlet, JSP
4. CA4: HTML, CSS, Bootstrap
5. CA5: CORS, Standard API, Spring, React, RIA
6. CA6: MySQL Database, JDBC
7. CA7: Authentication, Authorization(JWT), SQL Injection
8. CA8: Docker, Nginx
9. CA9: Kubernetes, CI/CD

## Screenshots

<img src="Screenshots/login.PNG" alt="Login" width="400"/>
<img src="Screenshots/reports.PNG" alt="Reports" width="400"/>
<img src="Screenshots/prerequisite.png" alt="Course List" width="400"/>
<img src="Screenshots/courses.png" alt="Selections" width="400"/>
<img src="Screenshots/schedule.png" alt="Schedule" width="400"/>
<img src="Screenshots/exit.PNG" alt="Exit" width="400"/>

## Developers

* [**Ghazal Kalhor**](https://github.com/kalhorghazal)
* [**Rasta Tadayon**](https://github.com/rastadayon)

